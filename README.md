# O que aprender antes do nodejs
 Teste
1. #### Lexical Structure
    - ##### Unicode  
      Javascript aceita a criação de variaveis com qualquer tipo de caracteres Unicode(Incluíndo emojis)  
      `let 💙 = '#goLoading';`
    - #####  Ponto e vírgula  
      No Javascript os pontos e virgula são despensáveis. Nesse caso, fica a cargo do desenvolvedor utilizar ou não.  
      `let nome = 'LoadingJr'` vai funcionar da mesma forma que `let nome = 'LoadingJr';` 
    - #####  Espaço em branco  
      O Javascript ignora espaços em branco no código.  
      O código `let         nome        =         "Jairo"` será executado sem nenhum problema, mas não é uma boa prática.  
      É recomendável utilizar formatadores de código para que o código fique bem organizado e identado de forma altomática. O mais usado atualmente é o [Prettier](https://prettier.io/).
    - #####  Maiúsculas e Minúsculas
      O Javascript é case-sensitive, ou seja, diferencia letras maúsculas e minúsculas  
      ```js
      let js;
      let Js;
      let JS;
      ```
      As variáveis acima são 3 variáveis diferentes.

    - #####  Comentários
      Comentários são trechos do código que serão ignorados durante a execução do arquivo.  
      Existem 2 tipos de comentários:
      - `//` é utilizado para comentar uma única linha;
      - `/**/` é utlizado para comentar uma ou mais linhas;  
      Exemplos: 
      ```js
      // este é um comentário de uma única linha
      /*
      Já este é um
      comentário de várias
      linhas
      */
      ``` 

    - #####  Literais e identificadores
      - Literal é um valor que está escrito no código fonte.  
      Eles podem ser por exemplo, um número, uma string, um booleano, uma matriz, ou um objeto;
      ```js
      3
      'Loading'
      true
      ['maçã', 'goiaba']
      {nome: 'Ruan', idade: 12}

      ```

      - O identificador é um conjunto de caracters que pode ser utilizado para identifica uma variável, uma função, ou um objeto. Ao criar um identificador, você poderá inicialo com uma letra, um `_` ou um `$`.  
      Identificadores válidos:
      ```js
      Test
      test
      TEST
      _test
      Test1
      $test
      ```

    - #####  Palavras reservadas
      Existe um conjunto palavras que são reservadas pela linguagem, e não podem ser utlizadas como identificadores.  
      São elas:
        - break
        - do
        - instanceof
        - typeof
        - case
        - else
        - new
        - var
        - catch
        - finally
        - return
        - void
        - continue
        - for
        - switch
        - while
        - debugger
        - function
        - this
        - with
        - default
        - if
        - throw
        - delete
        - in
        - try
        - class
        - enum
        - extends
        - super
        - const
        - export
        - import
        - implements
        - let
        - private
        - public
        - interface
        - package
        - protected
        - static
        - yield

2. #### Expressions

3. #### Types  
      No Javascript existem os seguinte tipos de dados:
      - **Boolean** - Aceita os valores `true` e `false`
      - **null** - Tipo que indica valor nulo
      - **undefined** - Tipo que indica valor indefinido
      - **Number** - Tipo para valores numéricos
      - **string** - Para caideia de caracteres
      - **object** - Tipo de dado complexo que permite armazenar coleções de dados por meio de chave-valor:
          `{chave: valor}`
          - Exemplo:
            ```js
            {
              nome: 'Pedro',
              profissao: 'desenvolvedor',
              idade: 25,
              cargo: 'presidente',
              ativo: true,
              cargosAnteriores: ['desenvolvedor', 'Diretor de Projetos'] 
            }
            ```

4. #### Classes  
      Classes em JavaScript são introduzidas no ECMAScript 2015 e são simplificações da linguagem para a utilização de heranças.  
      Por trás dos panos, classes são funções, que no ES2015(ou ES6) foram redesenhadas para facilitar a utilização de POO dentro do Javascript.

      Exemplo de Classe: 
      ```ts
        class Retangulo {
          constructor(altura, largura) {
            this.altura = altura; this.largura = largura;
          }
          //Getter
          get area() {
              return this.calculaArea()
          }

          calculaArea() {
              return this.altura * this.largura;
          }
        }
      ```
      ```js
        const quadrado = new Retangulo(10, 10);

        console.log(quadrado.area);
      ```

5. #### Variables  
      Variáveis são identificadores que guardam valores em posições da memória.  
      Existems 3 maneiras de declarar uma variável no JS.
      - **var** - Variáveis criadas com var possuem acesso fora do escopo(Depois do ES6, recomenda-se utilizar let em vez de var)
      - **let** - As variáveis criadas com let possuem um escopo fechado.
      - **const** - Os valores dessa variável não podem ser alterados após a primeira atribuição.

6. #### Functions
      Funções são procedimentos que podem ser executados varias vezes sem precisar repetir o código. São usadas para executar tarefas ou calcular valores.
      ```js
      // função
      function quadrado(numero) {
        return numero * numero;
      }
      // procedimento
      function exibeNumero(numero) {
        console.log(numero);
      }

      console.log(quadrado(2)) // 4
      
      exibeNumero(2) //2

      exibeNumero(quadrado(2)) //2
      
      ```

7. #### this

8. #### Arrow Functions
      Uma expressão arrow function possui uma sintaxe mais curta quando comparada a uma expressão de função e não tem seu próprio this. São indicadas para funções que não sejam métodos, e elas não podem ser usadas como construtoras de classes.  
      Exemplo:
      ```js
        // função comum
        function(element) {
          return element.length;
        }
        //arrow functions
        (element) => {return element.length;}

        (element, index) => element.length
        
        element => element.length
      ```

9. #### Loops
    - for
    ```js
    for (inicializador; condição-saída; expressão-final) {
      // código para executar
    }
    ```
    ```ts
      let cats = ['Bill', 'Jeff', 'Pete', 'Biggles', 'Jasmin'];
      let info = 'Meus gatos chamam-se ';
      
      for (let i = 0; i < cats.length; i++) {
        info += cats[i] + ', ';
      }

      console.log(info);
    ``` 
    - while
    ```js
      inicializador
      while (condição-saída) {
        // code to run

        expressão-final
      }
    ```
    ```ts
        let cats = ['Bill', 'Jeff', 'Pete', 'Biggles', 'Jasmin'];
        let info = 'Meus gatos chamam-se ';
        
        let i = 0;

        while (i < cats.length) {
          if (i === cats.length - 1) {
            info += 'and ' + cats[i] + '.';
          } else {
            info += cats[i] + ', ';
          }

          i++;
        }
    ``` 
    - dowhile
      ```js
      initializer
      do {
        // code to run

        final-expression
      } while (exit-condition)
      ```
      ```js
      var i = 0;

      do {
        if (i === cats.length - 1) {
          info += 'and ' + cats[i] + '.';
        } else {
          info += cats[i] + ', ';
        }

        i++;
      } while (i < cats.length);
      ```

10. #### Scopes
      Os escopos determinam a visibilidade das variáveis. Existem 3 tipos de escopos:
      - ##### Function scope
          Variáveis declaradas dentro de uma função não são visiveis fora do escopo dessa função. Independente se ele for criada com var, let ou const.

          ```js
          function myFunction() {
            let carName = "Volvo";   // Function Scope
          }
          ```
      - ##### Global scope
          Variáveis declaradas fora da função possuem um escopo global, ou seja, são visíveis tanto fora, como dentro da função.
          ```js
          let carName = "Volvo";
          // code here can use carName

          function myFunction() {
            console.log(carName)
            // code here can also use carName
          }
          ```

      - ##### Block scope
          Antes do ES6 só existiam os escopos globais e de funções. No ES6 foi criado o escopo de bloco, com uso dos declaradores let e const, onde a variável só é visível dentro de um determinado bloco {}.
          ```js
          {
            let x = 2;
          }
          // x can NOT be used here
          ```

11. Arrays

12. Template Literals

13. Semicolons

14. Strict Mode

15. ECMAScript 6, 2016, 2017

## Referências
- [Documentção Nodejs](https://nodejs.dev/learn/how-much-javascript-do-you-need-to-know-to-use-nodejs)  
- [Estruturas léxas do Javascript](https://flaviocopes.com/javascript-lexical-structure/)  
- [Tipos de dados](https://www.tutorialrepublic.com/javascript-tutorial/javascript-data-types.php)
- [Classes](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Classes)
- [Funções](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide/Functions)
- [Arrow Functions](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Functions/Arrow_functions)
- [Loops](https://developer.mozilla.org/pt-BR/docs/Learn/JavaScript/Building_blocks/Looping_code)
